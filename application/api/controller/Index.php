<?php

namespace app\api\controller;

use think\Controller;
use think\Request;

class Index extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $text = "布尔是PHP主要发祥地之一，也是中国古都php技术最高的大神，php教程古迹众多。";
        $this->speech(4,5,5,5,$text,'bool');
        $res =$this->reads('voice_config');
        dump($res);
    }


    /**
     * 语音合成
     * $per 发音人选择, 0为普通女声，1为普通男生，3为情感合成-度逍遥，4为情感合成-度丫丫，默认为普通女声
     * $spd 语速，取值0-15，默认为5中语速
     * $pit 音调，取值0-15，默认为5中语调
     * $vol 音量，取值0-9，默认为5中音量
     * $text 语音默认的内容为"欢迎使用百度语音合成"的urlencode,utf-8 编码
     * $voice 保存语音文件的名称
     */
    public function speech($per,$spd,$pit,$vol,$text,$voice)
    {
        $data=[
            'apiKey'=>'YALCmiV43CWBdUil5fs2PF9v'.',',
            'secretKey'=>'fxTPG9h6REr6nMCSIcS5Gnyk2BSfzccL'.',',
            'per'=>$per.',',
            'spd'=>$spd.',',
            'pit'=>$pit.',',
            'vol'=>$vol.',',
            'text'=>$text.',',
            '$voice'=>$voice
        ];
        $this->writes('voice_config',$data);
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
