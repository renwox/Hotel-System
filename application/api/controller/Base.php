<?php

namespace app\api\controller;

use think\Controller;
use think\Request;

class Base extends Controller
{
    /**
     * 写入日志文件
     * $name 写入的文件名
     * $data 需要写入的数据
     */
    public function writes($name,$data)
    {
        file_put_contents('./config/'.$name.'.txt',$data);
    }

    /**
     * 读取日志文件
     * $name 读取的文件名
     */
    public function reads($name)
    {
        $content = file_get_contents('./config/'.$name.".txt");
        $res = explode(",", $content);
        return $res;
    }


    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
