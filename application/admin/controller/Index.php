<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request;

class Index extends Base
{
    /**
     * 首页
     *
     * @return \think\Response
     */
    public function index()
    {
       return view();
    }

    /**
     * 默认页面
     *
     * @return \think\Response
     */
    public function home()
    {
        $this->query('hotel_house','list');
        return view();
    }
    /**
     * 入住管理
     *
     * @return \think\Response
     */
    public function check_in()
    {
        $this->query_find('hotel_house',[['id','=',input('id')]],'list');
        return view();
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
